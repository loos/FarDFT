set terminal epslatex color 
set output "texfile.tex"
set style data linespoints
### line styles
###
set style line 1 lt 1 lc rgb "red" lw 4
set style line 2 dt 3 lc rgb "red" lw 4
set style line 3 lt 1 lc rgb "dark-green" lw 4
set style line 4 dt 3 lc rgb "dark-green" lw 4
set style line 5 lt 1 lc rgb "dark-yellow" lw 4
set style line 6 dt 3 lc rgb "dark-yellow" lw 4
set style line 7 lt 1 lc rgb "dark-violet" lw 4
set style line 8 dt 3 lc rgb "dark-violet" lw 4
set style line 9 lt 1 lc rgb "blue" lw 4
set style line 10 dt 3 lc rgb "blue" lw 4
set style line 11 lt 1 lc rgb "dark-orange" lw 4
set style line 12 dt 3 lc rgb "dark-orange" lw 2
set style line 13 lt 1 lc rgb "black" lw 4
set style line 14 dt 3 lc rgb "black" lw 4
set style line 15 lt 1 lc rgb "purple" lw 4
set style line 16 dt 3 lc rgb "purple" lw 4
set style line 17 lt 1 lc rgb "#483D8B" lw 4
set style line 18 dt 3 lc rgb "#483D8B" lw 4
set style line 19 lt 1 lc rgb "#2ECCFA" lw 4
set style line 20 dt 3 lc rgb "#2ECCFA" lw 4
set style line 21 lt 1 lc rgb "#58FA82" lw 4
set style line 22 dt 3 lc rgb "#58FA82" lw 4
set style line 23 lt 1 lc rgb "#F5BCA9" lw 4
set style line 24 dt 3 lc rgb "#F5BCA9" lw 4
set style line 25 lt 1 lc rgb "#8A0829" lw 4
set style line 26 dt 3 lc rgb "#8A0829" lw 4

set tmargin 3
set bmargin 2
set lmargin 3
set rmargin 2

set format x '\tiny {%g}'
set format y '\tiny {%g}'

set multiplot layout 2,3 title "$E_w = (1-w)E_0 + w E_1 = fct(\\Delta v)$"

set title "$U/t = 1$" font "Helvetica,28" 
U=1
set xrange [-2*U:2*U]
set key font ",001"
set key horizontal bmargin
set key width 5
plot "../Ew1_Ew2_fctdeltav_Usurt0.0_w0.0" u 1:2 smooth csplines with lines ls 1 title "\\tiny $w = 0$",\
"../Ew1_Ew2_fctdeltav_Usurt1.0_w0.1" u 1:2 smooth csplines with lines ls 3 title "\\tiny $.1$",\
"../Ew1_Ew2_fctdeltav_Usurt1.0_w0.2" u 1:2 smooth csplines with lines ls 5 title "\\tiny $.2$",\
"../Ew1_Ew2_fctdeltav_Usurt1.0_w0.3" u 1:2 smooth csplines with lines ls 7 title "\\tiny $.3$",\
"../Ew1_Ew2_fctdeltav_Usurt1.0_w0.4" u 1:2 smooth csplines with lines ls 9 title "\\tiny $.4$",\
"../Ew1_Ew2_fctdeltav_Usurt1.0_w0.5" u 1:2 smooth csplines with lines ls 11 title "\\tiny $.5$";

set title "$U/t = 2$" font "Helvetica,28"
U=2
set xrange [-2*U:2*U]
unset key
plot "../Ew1_Ew2_fctdeltav_Usurt0.0_w0.0" u 1:2 smooth csplines with lines ls 1 title "$w = 0$",\
"../Ew1_Ew2_fctdeltav_Usurt2.0_w0.1" u 1:2 smooth csplines with lines ls 3 title "$w = 0.1$",\
"../Ew1_Ew2_fctdeltav_Usurt2.0_w0.2" u 1:2 smooth csplines with lines ls 5 title "$w = 0.2$",\
"../Ew1_Ew2_fctdeltav_Usurt2.0_w0.3" u 1:2 smooth csplines with lines ls 7 title "$w = 0.3$",\
"../Ew1_Ew2_fctdeltav_Usurt2.0_w0.4" u 1:2 smooth csplines with lines ls 9 title "$w = 0.4$",\
"../Ew1_Ew2_fctdeltav_Usurt2.0_w0.5" u 1:2 smooth csplines with lines ls 11 title "$w = 0.5$";

set title "$U/t = 5$" font "Helvetica,28"
unset key
U=5
set xrange [-2*U:2*U]
plot "../Ew1_Ew2_fctdeltav_Usurt5.0_w0.0" u 1:2 smooth csplines with lines ls 1 title "$w = 0$",\
"../Ew1_Ew2_fctdeltav_Usurt5.0_w0.1" u 1:2 smooth csplines with lines ls 3 title "$w = 0.1$",\
"../Ew1_Ew2_fctdeltav_Usurt5.0_w0.2" u 1:2 smooth csplines with lines ls 5 title "$w = 0.2$",\
"../Ew1_Ew2_fctdeltav_Usurt5.0_w0.3" u 1:2 smooth csplines with lines ls 7 title "$w = 0.3$",\
"../Ew1_Ew2_fctdeltav_Usurt5.0_w0.4" u 1:2 smooth csplines with lines ls 9 title "$w = 0.4$",\
"../Ew1_Ew2_fctdeltav_Usurt5.0_w0.5" u 1:2 smooth csplines with lines ls 11 title "$w = 0.5$";

set title "$U/t = 10$" font "Helvetica,28"
unset key
U=10
set xrange [-2*U:2*U]
plot "../Ew1_Ew2_fctdeltav_Usurt10.0_w0.0" u 1:2 smooth csplines with lines ls 1 title "$w = 0$",\
"../Ew1_Ew2_fctdeltav_Usurt10.0_w0.1" u 1:2 smooth csplines with lines ls 3 title "$w = 0.1$",\
"../Ew1_Ew2_fctdeltav_Usurt10.0_w0.2" u 1:2 smooth csplines with lines ls 5 title "$w = 0.2$",\
"../Ew1_Ew2_fctdeltav_Usurt10.0_w0.3" u 1:2 smooth csplines with lines ls 7 title "$w = 0.3$",\
"../Ew1_Ew2_fctdeltav_Usurt10.0_w0.4" u 1:2 smooth csplines with lines ls 9 title "$w = 0.4$",\
"../Ew1_Ew2_fctdeltav_Usurt10.0_w0.5" u 1:2 smooth csplines with lines ls 11 title "$w = 0.5$";

set title "$U/t = 100$" font "Helvetica,28"
unset key
U = 100
set xrange [-2*U:2*U]
plot "../Ew1_Ew2_fctdeltav_Usurt100.0_w0.0" u 1:2 smooth csplines with lines ls 1 title "$w = 0$",\
"../Ew1_Ew2_fctdeltav_Usurt100.0_w0.1" u 1:2 smooth csplines with lines ls 3 title "$w = 0.1$",\
"../Ew1_Ew2_fctdeltav_Usurt100.0_w0.2" u 1:2 smooth csplines with lines ls 5 title "$w = 0.2$",\
"../Ew1_Ew2_fctdeltav_Usurt100.0_w0.3" u 1:2 smooth csplines with lines ls 7 title "$w = 0.3$",\
"../Ew1_Ew2_fctdeltav_Usurt100.0_w0.4" u 1:2 smooth csplines with lines ls 9 title "$w = 0.4$",\
"../Ew1_Ew2_fctdeltav_Usurt100.0_w0.5" u 1:2 smooth csplines with lines ls 11 title "$w = 0.5$";

set title "$U/t = 1000$" font "Helvetica,28"
unset key
U = 1000
set xrange [-2*U:2*U]
plot "../Ew1_Ew2_fctdeltav_Usurt1000.0_w0.0" u 1:2 smooth csplines with lines ls 1 title "$w = 0$",\
"../Ew1_Ew2_fctdeltav_Usurt1000.0_w0.1" u 1:2 smooth csplines with lines ls 3 title "$w = 0.1$",\
"../Ew1_Ew2_fctdeltav_Usurt1000.0_w0.2" u 1:2 smooth csplines with lines ls 5 title "$w = 0.2$",\
"../Ew1_Ew2_fctdeltav_Usurt1000.0_w0.3" u 1:2 smooth csplines with lines ls 7 title "$w = 0.3$",\
"../Ew1_Ew2_fctdeltav_Usurt1000.0_w0.4" u 1:2 smooth csplines with lines ls 9 title "$w = 0.4$",\
"../Ew1_Ew2_fctdeltav_Usurt1000.0_w0.5" u 1:2 smooth csplines with lines ls 11 title "$w = 0.5$";

unset multiplot
