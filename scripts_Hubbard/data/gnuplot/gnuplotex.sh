cat >> final.tex  << '%EOF%'
\documentclass{article}
\usepackage{color,graphics,inputenc,amsmath}
\begin{document}
\pagenumbering{gobble}
\clearpage
\thispagestyle{empty}
\begin{figure}
\begin{center}
\input{texfile}
\end{center}
\end{figure}
\end{document}
%EOF%
pdflatex final.tex ;
rm final.tex | rm final.log | rm final.aux | rm texfile.tex | rm texfile.eps | rm texfile-eps-converted-to.pdf ;
pdfcrop --margins 10 final.pdf ;
mv -f final-crop.pdf final.pdf ;
